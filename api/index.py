from fastapi import Request, FastAPI

app = FastAPI()

@app.get("/api/python")
def hello_world():
    return {"Welcome Message": "Hallo, Welt."}

@app.post("/api/parens/english")
def english_to_prolog(request: Request):
    return request.json()